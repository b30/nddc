# Nedata
Netdata docker-compose.yml

Netdata: [Github](https://github.com/netdata/netdata) | [DockerHub](https://hub.docker.com/r/netdata/netdata)

»Netdata is high-fidelity infrastructure monitoring and troubleshooting. Open-source, free, preconfigured, opinionated, and always real-time.«